#!/bin/bash

source ./general.sh
source ./interfaces.sh
source ./menus.sh
source ./flush.sh
source ./input.sh
source ./output.sh
source ./forward.sh
source ./nat.sh


function main() {
	init_interfaces
	interface_menu
	items=$(configuration_menu)
	clear

	load_module
	
	for itm in $items;
	do
		case $itm in
			1)
				flush_rules
				;;
			2)
				flush_policy
				;;
			3)
				input_rules_state_tracking
				;;
			4)
				input_rules_anti_spoofing
				;;
			5)
				input_rules_accept
				;;
			6)
				input_rules_log
				;;
			7)
				output_rules_state_tracking
				;;
			8)
				output_rules_accept
				;;
			9)
				output_rules_log
				;;
			10)
				forward_rules_state_tracking
				;;
			11)
				forward_rules_anti_spoofing
				;;
			12)
				forward_rules_accept
				;;
			13)
				forward_rules_log
				;;
			14)
				nat_rules_pre_routing
				;;
			15)
				nat_rules_post_routing
				;;
			16)
				enable_forwarding_v4
				;;
			*)
				echo "Unrecognized option $itm"
				;;
		esac
	done

}

main
