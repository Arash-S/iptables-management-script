#!/bin/bash

declare -A INTERFACES

LO_DEV=lo
INT_DEV=
INT_NET_V4=
INT_NET_V6=

IPTABLES=$(whereis iptables | awk '{print $2}')
MODPROBE=$(whereis modprobe | awk '{print $2}')

HEIGHT=$(expr $(tput lines) - 5)
WIDTH=$(expr $(tput cols) - 50)
CHOICE_HEIGHT=$(expr $HEIGHT - 10)


OPTIONS=(
	1 "Flush the rules" on
	2 "Flush the policy" on
	3 "Input state tracking rules" off
	4 "Input anti spoofing rules" off
	5 "Input accept rules" off
	6 "Input log rules" off
	7 "Ouput state tracking rules" off
	8 "Output accept rules" off
	9 "Output log rules" off
	10 "Forward state tracking rules" off
	11 "Forward anti spoofing rules" off
	12 "Forward accept rules" off
	13 "Forward log rules" off
	14 "NAT pre routing rules" off
	15 "NAT post routing rules" off
	16 "Enable ip forwarding rules" off
)

function load_module() {
	echo "[+] Load Connection Tracking Modules..."
	
	$MODPROBE ip_conntrack
	$MODPROBE iptable_nat
	$MODPROBE ip_conntrack_ftp
	$MODPROBE ip_nat_ftp
}
