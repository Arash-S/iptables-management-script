#!/bin/bash

source ./general.sh


function output_rules_state_tracking() {
	echo "[+] Setting up OUTPUT state tracking rules..."

	$IPTABLES -A OUTPUT -m state --state INVALID -j LOG --log-prefix "DROP INVALID " --log-ip-options --log-tcp-options
	$IPTABLES -A OUTPUT -m state --state INVALID -j DROP
	$IPTABLES -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
}

function output_rules_accept() {
	echo "[+] Setting up OUTPUT accept rules..."

	$IPTABLES -A OUTPUT -p tcp --dport 21 --syn -m state --state NEW -j ACCEPT
	$IPTABLES -A OUTPUT -p tcp --dport 22 --syn -m state --state NEW -j ACCEPT
	$IPTABLES -A OUTPUT -p tcp --dport 25 --syn -m state --state NEW -j ACCEPT
	$IPTABLES -A OUTPUT -p tcp --dport 43 --syn -m state --state NEW -j ACCEPT
	$IPTABLES -A OUTPUT -p tcp --dport 80 --syn -m state --state NEW -j ACCEPT
	$IPTABLES -A OUTPUT -p tcp --dport 443 --syn -m state --state NEW -j ACCEPT
	$IPTABLES -A OUTPUT -p tcp --dport 4321 --syn -m state --state NEW -j ACCEPT
	$IPTABLES -A OUTPUT -p tcp --dport 53 -m state --state NEW -j ACCEPT
	
	$IPTABLES -A OUTPUT -p udp --dport 53 -m state --state NEW -j ACCEPT

	$IPTABLES -A OUTPUT -p icmp --icmp-type echo-request -j ACCEPT
}

function output_rules_log() {
	echo "[+] Setting up OUTPUT log rules..."

	$IPTABLES -A OUTPUT ! -o $LO_DEV -j LOG --log-prefix "DROP " --log-ip-options --log-tcp-options
}
