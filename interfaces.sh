#!/bin/bash

source ./general.sh


# Setup associative array for network interfaces
# array keys are interface name and 
# values are string that contain interface name, ipv4 and ipv6
function init_interfaces() {
	padding=$(expr $(expr $(tput cols) - 50) / 3)
	
	for int in $(ip -o a | cut -d ' ' -f 2 | sort -u | sed 's/://g');
	do
		v4=$(ip -4 address show dev $int |
			awk '/inet/ {print $2}'	 |
			sed -n 1p)
		subnet=$(echo $v4 | awk -F/ '{print $2}')
		v4=$(echo $v4 | awk -F. '{print $1"."$2"."$3".0"}')
		v4=$(echo "$v4/$subnet")

		v6=$(ip -6 address show dev $int |
			awk '/inet6/ {print $2}' |
			sed -n 1p)

		if [ -z "$v4" ]; 
		then
			v4=$(printf "%-18s" "-")
		elif [ -z "$v6" ];
		then
			v6=$(printf "%-32s" "-")
		fi

		INTERFACES[$int]=$(printf "%-${padding}s %-${padding}s %-${padding}s\n" $int $v4 $v6)
	done
}
