#!/bin/bash

source ./general.sh


function interface_menu() {
	back_title="IPTable Configuration"
	title="Network Interfaces"
	menu="Choose one of the following network interfaces:"

	options=()
	for key in "${INTERFACES[@]}";
	do
		options+=("$key" "${INTERFACES[${key}]}")
	done

	choice=$(dialog --clear --backtitle "$back_title" \
		--title "$title" --menu "$menu" \
		$HEIGHT $WIDTH $CHOICE_HEIGHT \
		"${options[@]}" 2>&1 >/dev/tty)

	clear
	INT_DEV=$(echo $choice | cut -d ' ' -f 1)
	INT_NET_V4=$(echo $choice | cut -d ' ' -f 2)
	INT_NET_V6=$(echo $choice | cut -d ' ' -f 3)
}

function configuration_menu() {
	back_title="IPTable Configuration"
	title="Operations"
	menu="Choose one of the following operation:"

	choice=$(dialog --clear --backtitle "$back_title" \
		--title "$title" --checklist "$menu" \
		$HEIGHT $WIDTH $CHOICE_HEIGHT \
		"${OPTIONS[@]}" 2>&1 >/dev/tty)

	clear
	echo $choice
}
