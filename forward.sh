#!/bin/bash

source ./general.sh


function forward_rules_state_tracking() {
	echo "[+] Setting up forward state tracking rules..."

	$IPTABLES -A FORWARD -m state --state INVALID -j LOG --log-prefix "DROP INVALID " --log-ip-options --log-tcp-options
	$IPTABLES -A FORWARD -m state --state INVALID -j DROP
	$IPTABLES -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
}

function forward_rules_anti_spoofing() {
	echo "[+] Setting up forward anti spoofing rules..."

	$IPTABLES -A FORWARD -i $INT_DEV ! -s $INT_NET_V4 -j LOG --log-prefix "SPOOFED PKT V4 "
	$IPTABLES -A FORWARD -i $INT_DEV ! -s $INT_NET_V4 -j DROP
}

function forward_rules_accept() {
	echo "[+] Setting up forward accept rules..."

	$IPTABLES -A FORWARD -p tcp -i $INT_DEV -s $INT_NET_V4 --dport 21 --syn -m state --state NEW -j ACCEPT
	$IPTABLES -A FORWARD -p tcp -i $INT_DEV -s $INT_NET_V4 --dport 22 --syn -m state --state NEW -j ACCEPT
	$IPTABLES -A FORWARD -p tcp -i $INT_DEV -s $INT_NET_V4 --dport 25 --syn -m state --state NEW -j ACCEPT
	$IPTABLES -A FORWARD -p tcp -i $INT_DEV -s $INT_NET_V4 --dport 43 --syn -m state --state NEW -j ACCEPT
	
	$IPTABLES -A FORWARD -p tcp --dport 80 --syn -m state --state NEW -j ACCEPT
	$IPTABLES -A FORWARD -p tcp --dport 443 --syn -m state --state NEW -j ACCEPT
	
	$IPTABLES -A FORWARD -p tcp -i $INT_DEV -s $INT_NET_V4 --dport 4321 --syn -m state --state NEW -j ACCEPT 
	
	$IPTABLES -A FORWARD -p udp --dport 53 -m state --state NEW -j ACCEPT
	$IPTABLES -A FORWARD -p tcp --dport 53 -m state --state NEW -j ACCEPT

	$IPTABLES -A FORWARD -p icmp --icmp-type echo-request -j ACCEPT
}

function forward_rules_log() {
	echo "[+] Setting up forward log rules..."

	$IPTABLES -A FORWARD ! -i $LO_DEV -j LOG --log-prefix "DROP " --log-ip-options --log-tcp-options
}

function enable_forwarding_v4() {
	echo "[+] Enable IP forwarding..."
	echo 1 > /proc/sys/net/ipv4/ip_forward
}
