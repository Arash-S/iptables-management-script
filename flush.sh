#!/bin/bash

source ./general.sh

function flush_rules() {
	echo "[+] Flushing existing iptables rules..."
	
	$IPTABLES -F
	$IPTABLES -F -t nat
	$IPTABLES -X
}

function flush_policy() {
	echo "[+] Flushing existing iptables policy..."
	
	$IPTABLES -P INPUT DROP
	$IPTABLES -P OUTPUT DROP
	$IPTABLES -P FORWARD DROP
}
