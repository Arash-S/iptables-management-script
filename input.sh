#!/bin/bash

source ./general.sh


function input_rules_state_tracking() {
	echo "[+] Setting up INPUT state tracking rules..."
	
	$IPTABLES -A INPUT -m state --state INVALID -j LOG --log-prefix "DROP INVALID " --log-ip-options --log-tcp-options
	$IPTABLES -A INPUT -m state --state INVALID -j DROP
	$IPTABLES -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
}

function input_rules_anti_spoofing() {
	echo "[+] Setting up INPUT anti spoofing rules..."

	$IPTABLES -A INPUT -i $INT_DEV ! -s $INT_NET_V4 -j LOG --log-prefix "SPOOFED PKT V4"
	$IPTABLES -A INPUT -i $INT_DEV ! -s $INT_NET_V4 -j DROP
}

function input_rules_accept() {
	echo "[+] Setting up INPUT accept rules..."

	$IPTABLES -A INPUT -i $INT_DEV -p tcp -s $INT_NET_V4 --dport 22 --syn -m state --state NEW -j ACCEPT
	$IPTABLES -A INPUT -p icmp --icmp-type echo-request -j ACCEPT
}

function input_rules_log() {
	echo "[+] Setting up INPUT log rules..."

	$IPTABLES -A INPUT ! -i $LO_DEV -j LOG --log-prefix "DROP " --log-ip-options --log-tcp-options
}
